package com.github.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan(basePackages = "${spring.componentScan.basePackages}")
public class AppConfig extends WebMvcConfigurerAdapter {
}
