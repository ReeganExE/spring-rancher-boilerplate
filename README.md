# Spring Boot Boilerplate

```sh
mvn clean install
```

## Deploy

```sh
rancher-compose \
  --url https://server_ip:8080 \
  --access-key <key> \
  --secret-key <secret-key> \
  -f ./spring-rancher/deployment/docker-compose.yml \
  -r ./spring-rancher/deployment/rancher-compose.yml \
  up -d

```